<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Applications extends Model
{
    protected $table = 'applications';
    //What fields are mass fillable
    //protected $fillable = ['title', 'description'];
    //This stops anyone from editing these fields
    protected $guarded = [];

    protected static function boot() {
        parent::boot();

        //runs after project has been created and sent into the database
        // static::created(function($project) {
        //     Mail::to($project->owner->email)->send(
        //         new ProjectCreated($project)
        //     );
        // });
    }

    public function attachUsers()
    {
        return $this->leftJoin('users AS re', 're.id', '=', 'reviewed_by')
                    ->leftJoin('users AS up', 'up.id', '=', 'updated_by')
                    ->select('applications.*','re.name as reviewed_by', 'up.name as updated_by')
                    ->get();
    }

    public function updateRecord($data, $action, $id) {
        $record = $this->find($id);

        if( $action == 'Update Application') {
            $record->business_name = $data['business_name'];
            $record->first_name = $data['fname'];
            $record->last_name = $data['lname'];
            $record->email = $data['email'];
            $record->ein = $data['ein'];
            $record->resale_number = $data['resale'];
            $record->cosmo_number = $data['cosmo'];
            $record->telephone = $data['telephone'];
            $record->updated_by = $data['updatedBy'];
            $record->street = $data['street'];
            $record->city = $data['city'];
            $record->state = $data['state'];
            $record->zip = $data['zip'];
            $record->updated_at = now();
        } else {
            $record->reviewed_by = $data['reviewedBy'];
            $record->status = $data['status'];
            $record->updated_by = $data['updatedBy'];
            $record->updated_at = now();
        }

        $record->save();

        return true;
    }
}
