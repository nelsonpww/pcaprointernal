<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class MagentoUserCreatedNotification extends Mailable
{
    use Queueable, SerializesModels;

    public function __construct($newRecord)
    {
         $this->newRecord = $newRecord;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from('noreply@pcapro.com')
                    ->view('emails.apps.newMagentoUser')
                    ->with('data', $this->newRecord);
    }
}
