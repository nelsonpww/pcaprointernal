<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UploadFile extends Model
{
    protected $table = 'upload_files';
    //What fields are mass fillable
    //protected $fillable = ['title', 'description'];
    //This stops anyone from editing these fields
    protected $guarded = [];

    protected static function boot() {
        parent::boot();

        //runs after project has been created and sent into the database
        // static::created(function($project) {
        //     Mail::to($project->owner->email)->send(
        //         new ProjectCreated($project)
        //     );
        // });
    }

}
