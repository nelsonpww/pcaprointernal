<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Magento extends Model
{
    protected $table = 'magento_users';
    //What fields are mass fillable
    //protected $fillable = ['title', 'description'];
    //This stops anyone from editing these fields
    protected $guarded = [];

    protected static function boot() {
        parent::boot();

        //runs after project has been created and sent into the database
        // static::created(function($project) {
        //     Mail::to($project->owner->email)->send(
        //         new ProjectCreated($project)
        //     );
        // });
    }

    public function getApplicationUsers(){
        return $this->leftJoin('applications AS app', 'app.id', '=', 'app_id')
                    ->get([
                        'magento_users.*',
                        'app.id',
                        'app.business_name'
                    ]);
    }

    public function getSingleUser($id){
        return $this->leftJoin('applications AS app', 'app.id', '=', 'app_id')
                    ->where('user_id', $id)
                    ->get([
                        'magento_users.*',
                        'app.id',
                        'app.business_name'
                    ])->first();
    }
}
