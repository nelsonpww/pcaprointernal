<?php

namespace App\Services;

use Illuminate\Http\Request;

use GuzzleHttp\Client;
use App\Magento;

class Generator
{
    public function generateAuthToken()
    {
        $client = new Client([
            'base_uri' => 'https://pcapro.com/shop/rest/V1/',
        ]);

        $magentoAdmin = env('MANGETO_USERNAME');
        $magentoPass = env('MANGETO_PASSWORD');
        $adminlogin = '{"username": "'.$magentoAdmin.'","password": "'.$magentoPass.'"}';

        $response = $client->request('POST', 'integration/admin/token', [
             'http_errors' => false,
             'headers' => [
                 'Content-Type' => 'application/json',
             ],
             'body' => $adminlogin
        ]);
        $status = $response->getStatusCode();
        if( $status == '200') {
             return $body = (string) $response->getBody();
        }
    }

    public function createMagentoCustomer()
    {
        $token = $this->generateAuthToken();
        $customerObj = (object) [];

        $customerObj->customer = [
            "group_id" => "1",
            "email" => "mail2@mail2.com",
            "firstname" => "Bob",
            "lastname" => "Billy",
            "store_id" => "1",
            "website_id" => "1",
        ];
        $customerObj->password = 'YesAndNo!#@';
        $customerJson = json_encode($customerObj);

        $client = new Client([
            'base_uri' => 'https://pcapro.com/shop/rest/V1/',
        ]);
        $response = $client->request('POST', 'customers', [
            'http_errors' => true,
            'headers' => [
                'Content-Type' => 'application/json',
                'Authorization' => $token,
            ],
            'body' => $customerJson
        ]);
    }

}


?>
