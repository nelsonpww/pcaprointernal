<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Mail\NewAppNotification;

class NotificationsController extends Controller
{
    public function newApplication(){
        Mail::to('nelson@perfumeworldwide.com')->send(new NewAppNotification);
    }
}
