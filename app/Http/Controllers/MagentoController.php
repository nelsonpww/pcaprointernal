<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\Mail;

use GuzzleHttp\Client;
use App\Magento;
use App\Applications;
use App\Mail\MagentoUserCreatedNotification;

use App\Http\Controllers\Controller as Controller;

class MagentoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
     /**
      * success response method.
      *
      * @return \Illuminate\Http\Response
      */
     public function generateAuthToken()
     {

         $client = new Client([
             'base_uri' => 'https://pcapro.com/shop/rest/V1/',
         ]);
         // if($cusUser == NULL && $cusPass == NULL) {
             $magentoAdmin = env('MANGETO_USERNAME');
             $magentoPass = env('MANGETO_PASSWORD');
             $magentoPath = 'integration/admin/token';
         // } else {
         //     $magentoAdmin = $cusUser;
         //     $magentoPass = $cusPass;
         //     $magentoPath = 'integration/customer/token';
         // }
         $adminlogin = '{"username": "'.$magentoAdmin.'","password": "'.$magentoPass.'"}';

         $response = $client->request('POST', $magentoPath, [
              'http_errors' => false,
              'headers' => [
                  'Content-Type' => 'application/json',
              ],
              'body' => $adminlogin
         ]);

         $status = $response->getStatusCode();

         if( $status == '200') {
             $token = $response->getBody()->getContents();
             $cleanToken = str_replace('"','',$token);
             return $cleanToken;
         }

     }

     public function getCountryRegions($token)
     {

         $client = new Client([
                 'base_uri' => 'https://pcapro.com/shop/rest/V1/',
             ]);
             $response = $client->request('GET', 'directory/countries/US', [
                 'http_errors' => true,
                 'headers' => [
                     'Content-Type' => 'application/json',
                     'Authorization' => 'Bearer '.$token,
                 ]
             ]);
             return json_decode($response->getBody()->getContents(), true);
     }

     public function createMagentoCustomer(Request $request)
     {
        $info = $request->all();
        $appInfo = Applications::find($info['appId']);

        $token = $this->generateAuthToken();

        $customerObj = (object) [];

        $customerObj->customer = [
                "group_id" => "1",
                "email" => $info['email'],
                "firstname" => $info['fname'],
                "lastname" => $info['lname'],
                "store_id" => "1",
                "website_id" => "1",
                "addresses" =>[
                        [
                        "id" => $info['appId'],
                        "firstname" => $info['fname'],
                        "lastname" => $info['lname'],
                        "company" => $appInfo->business_name,
                        "street" => [
                            $appInfo->street,
                            ],
                        "city" => $appInfo->city,
                        "region" => $appInfo->state_name,
                        "region_id" => $appInfo->state,
                        "postcode" => $appInfo->zip,
                        "telephone" => $appInfo->telephone,
                        "country_id" => "US",
                        "default_shipping" => true,
                        "default_billing" => true
                        ]
                    ]
            ];
            $stripped = str_replace(' ', '', $info['business_name']);
            $pass = strtolower($stripped);
            $pass = ucfirst($pass);

        $customerObj->password = $pass . '1234!';

        $customerJson = json_encode($customerObj);

        $client = new Client([
                'base_uri' => 'https://pcapro.com/shop/rest/V1/',
            ]);
            $response = $client->request('POST', 'customers', [
                'http_errors' => false,
                'headers' => [
                    'Content-Type' => 'application/json',
                    'Authorization' => 'Bearer '.$token,
                ],
                'body' => $customerJson
            ]);

            if( $response->getStatusCode() == 200) {
                $magentoData = json_decode($response->getBody()->getContents(), true);

                $newRecord['user_id'] = $magentoData['id'];
                $newRecord['firstname'] = $magentoData['firstname'];
                $newRecord['lastname'] = $magentoData['lastname'];
                $newRecord['group_id'] = $magentoData['group_id'];
                $newRecord['created_in'] = 'Default Store View';
                $newRecord['email'] = $magentoData['email'];
                $newRecord['password'] = $pass . '1234!';
                $newRecord['store_id'] = $magentoData['store_id'];
                $newRecord['website_id'] = $magentoData['website_id'];
                $newRecord['app_id'] = $info['appId'];

                $bool = Magento::Create($newRecord);

                Mail::to($newRecord['email'])->send(new MagentoUserCreatedNotification($newRecord));

                if($bool) {
                    $request->session()->flash('msgTitle', 'Success');
                    $request->session()->flash('msg', 'User has been generated in magento successfully! - The login credentials have been emailed to them.');
                    return redirect()->back();
                }
            } else {
                $body = json_decode($response->getBody(), true);

                $request->session()->flash('msgTitleE', 'ERROR:');
                $request->session()->flash('msgE', $body['message']);
                return redirect()->back()->withInput();
            }
     }

     public function resetCustomerPassword(Request $request)
     {
         $info = $request->all();
         $token = $request->session()->get('magentoKey');

         $customerObj = (object) [];
         $customerObj = [
                "email" => $info['email'],
                "template" => 'email_reset',
                "website_id" => "1",
            ];
        $customerJson = json_encode($customerObj);

        $client = new Client([
                'base_uri' => 'https://pcapro.com/shop/rest/V1/',
            ]);
            $response = $client->request('PUT', 'customers/password', [
                'http_errors' => true,
                'headers' => [
                    'Content-Type' => 'application/json',
                    'Authorization' => 'Bearer '.$token,
                ],
                'body' => $customerJson
            ]);

            if( $response->getStatusCode() == 200) {
                $request->session()->flash('msgTitle', 'Success');
                $request->session()->flash('msg', 'Magento password reset email sent successfully! May take up to 1-2hours');
                return redirect()->back();
            } else {
                $body = json_decode($response->getBody(), true);

                $request->session()->flash('msgTitleE', 'ERROR:');
                $request->session()->flash('msgE', $body['message']);
                return redirect()->back()->withInput();
            }
     }

     public function updateMagentoCustomer(Request $request)
     {
         $info = $request->all();
         $token = $request->session()->get('magentoKey');
         $magId = $info['magId'];

         $customerObj = (object) [];

         $customerObj->customer = [
                "id" => $magId,
                "email" => $info['email'],
                "firstname" => $info['fname'],
                "lastname" => $info['lname'],
                "website_id" => "1",
                "addresses" =>[
                        [
                        "id" => $info['address_id'],
                        "customer_id" => $magId,
                        "firstname" => $info['fname'],
                        "lastname" => $info['lname'],
                        "company" => $info['company_name'],
                        "street" => [
                            $info['street'],
                            ],
                        "city" => $info['city'],
                        "region" => [
                            "region_id" => $info['state']
                        ],
                        "postcode" => $info['zip'],
                        "telephone" => $info['telephone'],
                        "country_id" => "US",
                        "default_shipping" => true,
                        "default_billing" => true
                        ]
                    ]
            ];

        $customerJson = json_encode($customerObj);

        $client = new Client([
                'base_uri' => 'https://pcapro.com/shop/rest/V1/',
            ]);
            $response = $client->request('PUT', 'customers/'.$magId, [
                'http_errors' => false,
                'headers' => [
                    'Content-Type' => 'application/json',
                    'Authorization' => 'Bearer '.$token,
                ],
                'body' => $customerJson
            ]);

            if( $response->getStatusCode() == 200) {
                $request->session()->flash('msgTitle', 'Success');
                $request->session()->flash('msg', 'Magento Information has been updated successfully!');
                return redirect()->back();
            } else {
                $body = json_decode($response->getBody(), true);

                $request->session()->flash('msgTitleE', 'ERROR:');
                $request->session()->flash('msgE', $body['message']);
                return redirect()->back()->withInput();
            }
     }

     public function getCustomerOrders($token, $magEmail)
     {
         // $token = $this->generateAuthToken("mail@mail.comw" ,"Nyllc1234!");
         //$token = $this->generateAuthToken();

         $client = new Client([
             'base_uri' => 'https://pcapro.com/shop/rest/V1/',
         ]);

         $response = $client->request('GET', 'orders?searchCriteria[filterGroups][0][filters][0][field]=customer_email&searchCriteria[filterGroups][0][filters][0][value]='.$magEmail, [
         //$response = $client->request('GET', 'orders?searchCriteria[sortOrders][0][field]=created_at', [
         // $response = $client->request('GET', 'customers/30', [
             'http_errors' => true,
             'headers' => [
                 'Content-Type' => 'application/json',
                 'Authorization' => 'Bearer '.$token,
             ]
         ]);
         $data = $response->getBody()->getContents();
         return json_decode($data);
     }

     public function getCustomerInfo($token, $magId)
     {
         // $token = $this->generateAuthToken("mail@mail.comw" ,"Nyllc1234!");
         //$token = $this->generateAuthToken();

         $client = new Client([
             'base_uri' => 'https://pcapro.com/shop/rest/V1/',
         ]);

         $response = $client->request('GET', 'customers/'.$magId, [
             'http_errors' => true,
             'headers' => [
                 'Content-Type' => 'application/json',
                 'Authorization' => 'Bearer '.$token,
             ]
         ]);
         $data = $response->getBody()->getContents();
         return json_decode($data);
     }

 }
