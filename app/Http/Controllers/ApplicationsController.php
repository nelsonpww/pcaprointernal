<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use App\Mail\NewAppNotification;
use App\Mail\ApprovedAppNotification;
use App\Mail\DeniedAppNotification;

use App\Applications;
use App\User;
use App\Magento;
use App\UploadFile;

class ApplicationsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

     public function __construct() {
         return $this->applicationModel = new Applications();
     }

    public function index(Request $request)
    {
        $apps = $this->applicationModel->attachUsers();
        return view('applications/index', compact('apps'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        $app = Applications::find($id);
        $form = UploadFile::find($app->file_id);
        $magUsers = Magento::where('app_id',$id)->get();
        $regions = app('App\Http\Controllers\MagentoController')->getCountryRegions($request->session()->get('magentoKey'));

        return view('applications.edit', compact('app', 'magUsers', 'form', 'regions'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id, Applications $Applications)
    {
        $action = $request->get('action');
        $currentUser = Auth::id();
        $appInfo = $request->all();

        if( $action == "Approve") {
            $data['status'] = 1;
            $data['reviewedBy'] = $currentUser;
            $data['updatedBy'] = $currentUser;
            Mail::to($appInfo['email'])->send(new ApprovedAppNotification($appInfo));
        }

        if( $action == "Deny") {
            $data['status'] = 2;
            $data['reviewedBy'] = $currentUser;
            $data['updatedBy'] = $currentUser;
            Mail::to($appInfo['email'])->send(new DeniedAppNotification($appInfo));
        }

        if( $action == "Update And Reset Denied Status") {
            $data = $request->all();
            $data['reviewedBy'] = $currentUser;
            $data['updatedBy'] = $currentUser;
            $data['status'] = NULL;
        }

        if( $action == "Update Application") {
            $data = $request->all();
            $data['updatedBy'] = $currentUser;
        }

        $bool = $Applications->updateRecord($data, $action, $id);

        if($bool){
            return redirect()->route('applications.edit', $id);
        } else {
            dd('SOMETHINGS BROKEN');
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function apply(Request $request)
    {
        $token = $request->session()->get('magentoKey');
        $regions = app('App\Http\Controllers\MagentoController')->getCountryRegions($token);
        return view('apply', compact('regions'));
    }

    public function sendApplyForm(Request $request)
    {
        $validatedData = $request->validate([
            'firstName' => 'required|alpha',
            'lastName' => 'required|alpha',
            'email' => 'required',
            'business' => 'required|unique:applications,business_name',
            'telephone' => 'required',
            'address_street' => 'required',
            'address_city' => 'required',
            'address_state' => 'required',
            'address_zip' => 'required',
            'ein' => 'required',
            'w9' => 'required|mimes:pdf',
        ]);

        $file = $request->file('w9');
        //$path = Storage::putFile('', $request->file('w9'));
        $filename = $file->getClientOriginalName();
        //$extension          = $file->getClientOriginalExtension();
        $size = $file->getClientSize();
        $path = Storage::putFile('', $request->file('w9'));

        $fileInfo = [
            'file_name' => $filename,
            'file_size' => $size,
            'file_path' => $path
        ];
        $bool1 = UploadFile::create($fileInfo);

        $data = [
            'first_name' => $request->firstName,
            'last_name' => $request->lastName,
            'email' => $request->email,
            'business_name' => $request->business,
            'street' => $request->address_street,
            'city' => $request->address_city,
            'state' => $request->address_state,
            'zip' => $request->address_zip,
            'telephone' => $request->telephone,
            'resale_number' => $request->resale,
            'cosmo_number' => $request->cosemtology,
            'ein' => $request->ein,
            'file_id' => $bool1->id
        ];
        $bool2 = Applications::create($data);

        if($bool1 && $bool2) {
            Mail::to('nelson@perfumeworldwide.com')->send(new NewAppNotification($data));
            return redirect("/apply")->withSuccess('Form has been submitted. Please allow 24-48 hours for a specialist to reach out to you.');
        } else {
            dd('error here');
        }
    }

    public function down(Request $request){
        return Storage::download($request->input('w9form'), 'w9form.pdf');
    }

}
