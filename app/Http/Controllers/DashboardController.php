<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

use App\Mail\NewAppNotification;

use App\Applications;
use App\User;
use App\Magento;

class DashboardController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function testFile(Request $request)
    {
        $file = $request->file('w9');
        $path = Storage::putFile('', $request->file('w9'));
        echo $path;
        dd($file->getClientOriginalName());
    }

    public function index(Request $request)
    {

        $user = Auth::user();
        $currentUser = User::find($user->id);

        $magentoKey = app('App\Http\Controllers\MagentoController')->generateAuthToken();

        $request->session()->put('name', $currentUser->name);
        $request->session()->put('email', $currentUser->email);
        $request->session()->put('magentoKey', $magentoKey);

        $newApps = Applications::where('status','=', NULL)->count();
        $approvedApps = Applications::where('status','=', 1)->count();
        $deniedApps = Applications::where('status','=', 2)->count();
        $activeMagento = Magento::where('deleted_at','=', NULL)->count();

        return view('dashboard/index', compact('newApps','approvedApps','deniedApps', 'activeMagento'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    public function logout()
    {
        Auth::logout();
    }
}
