<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Auth;

use App\Applications;
use App\Magento;
use App\Mail\ApproveApp;
//use App\Services\magentoApi\Endpoints;

class UsersController extends Controller
{
    public function index(Magento $magento)
    //public function index(Endpoints $api)
    {
        $magUsers = $magento->getApplicationUsers();
        return view('users.index', compact('magUsers'));
    }

    public function create(){}
    public function store(){}
    public function show(){}

    public function edit(Request $request, Magento $magento, $id){

        $magentoUser = $magento->getSingleUser($id);
        $magId = $magentoUser->user_id;
        $magEmail = $magentoUser->email;

        $token = $request->session()->get('magentoKey');

        $customerInfo = app('App\Http\Controllers\MagentoController')->getCustomerInfo($token, $magId);
        //dd($customerInfo);
        $orders = app('App\Http\Controllers\MagentoController')->getCustomerOrders($token, $magEmail);
        $regions = app('App\Http\Controllers\MagentoController')->getCountryRegions($token);

        //dd($regions);

        return view('users.edit', compact('magentoUser', 'orders', 'customerInfo', 'regions'));
    }

    public function update(){}
    public function destroy(){}
    public function generateMagentoUser()
    {

    }
}
