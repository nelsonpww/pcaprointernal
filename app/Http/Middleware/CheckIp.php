<?php

namespace App\Http\Middleware;

use Closure;

class CheckIp
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        return $next($request);

        // if( $request->ip() == '47.19.239.34'){
        //     return $next($request);
        // } else {
        //     return redirect()->route('error404');
        // }
    }
    
}
