@extends('layouts.dashboard')

@section('content')
<div class="container-fluid">
    <div class="row pt-4 pb-4">
        <div class="col-12">
            <h1 class="m-0 text-dark">All Applications</h1>
        </div>
    </div>
</div>

<div class="container-fluid">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                  <h3 class="card-title">All</h3>
                </div>
                <div class="card-body">
                    <table id="table_id" class="table table-striped table-bordered dt-responsive nowrap" style="width:100%">
                        <thead>
                            <tr>
                                <th class="no-sort">Name</th>
                                <th>Business Name</th>
                                <th>Telephone</th>
                                <th>Applied At</th>
                                <th>Reviewed By</th>
                                <th>File Status</th>
                                <th>Updated By</th>
                                <th>Updated At</th>
                                <th class="text-center">Action</th>
                            </tr>
                        </thead>
                        <tbody>

                            @foreach ($apps as $app)
                            <tr>
                                <td>{{ $app->first_name }} {{ $app->last_name }}</td>
                                <td>{{ $app->business_name }}</td>
                                <td>{{ $app->telephone }}</td>
                                <td>{{ $app->created_at->format('m/d/Y h:i a') }}</td>
                                <td>{{ $app->reviewed_by }}</td>
                                <td class="text-center">
                                <?php if($app->status == 0): ?>
                                    <a class="btn btn-warning disabled" href="{{route('applications.edit', $app->id)}}">Pending</a>
                                <?php elseif($app->status == 1): ?>
                                    <a class="btn btn-success disabled" href="{{route('applications.edit', $app->id)}}">Approved</a>
                                <?php elseif($app->status == 2): ?>
                                    <a class="btn btn-danger disabled" href="{{route('applications.edit', $app->id)}}">Denied</a>
                                <?php endif; ?>
                                </td>
                                <td>{{ $app->updated_by }}</td>
                                <td><?php echo isset( $app->updated_at) ? $app->updated_at->format('m/d/Y h:i a') : '';?></td>
                                <td class="text-center">
                                    <a class="btn btn-primary" href="{{route('applications.edit', $app->id)}}"><i class="glyphicon glyphicon-align-left"></i> View</a>
                                    <!-- <a class="btn btn-danger" href="#"><i class="glyphicon glyphicon-align-left"></i> Delete</a> -->
                                </td>
                            </tr>
                            @endforeach

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
