@extends('layouts.dashboard')

@section('content')

@if(  Session::has('msg') )
<div class="alert alert-primary" role="alert">
    <strong>{{ Session::get('msgTitle') }}</strong> {{ Session::get('msg') }}
</div>
@endif

@if( Session::has('msgE') )
<div class="alert alert-danger" role="alert">
    <strong>{{ Session::get('msgTitleE') }}</strong> {{ Session::get('msgE') }}
</div>
@endif


<div class="container-fluid">
    <div class="row pt-4 pb-4">
        <div class="col-12">
            <h1 class="m-0 text-dark">Application For: {{$app->business_name}}</h1>
        </div>
    </div>
</div>

<div class="container-fluid">
    <div class="row">
        <div class="col-10 offset-sm-1">
            <div class="card card-warning">
                <div class="card-header">
                    <h3 class="card-title font-weight-bold">Created On: {{$app->created_at->format('m/d/Y')}}</h3>

                    @if ($app->status == 0)
                    <div class="btn disabled btn-warning btn-md float-right">Pending</div>
                    @endif

                    @if ($app->status == 1)
                    <div class="btn disabled btn-success btn-md float-right">Approved</div>
                    @endif

                    @if ($app->status == 2)
                    <div class="btn disabled btn-danger btn-md float-right">Denied</div>
                    @endif

                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-12 mb-2">
                            <form action="{{route('down')}}" method="post">
                                @csrf
                                <input type="hidden" name="w9form" value="{{$form->file_path}}">
                                <button type="submit" class="btn btn-primary"><i class="fa fa-download"></i> Download W9 File</button>
                           </form>
                       </div>
                   </div>

                    <form method="POST" action="{{route('applications.update', $app->id)}}">
                        <input type="hidden" name="_method" value="PUT">
                        @csrf
                        <div class="row">
                            <div class="col-6">
                                <label>Business Name</label>
                                <input type="text" class="form-control" name="business_name" value="{{$app->business_name}}">
                            </div>
                            <div class="col-6">
                                <label>Telephone Number</label>
                                <input type="text" class="form-control" name="telephone" value="{{$app->telephone}}">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>First Name</label>
                                    <input type="text" class="form-control" name="fname" value="{{$app->first_name}}">
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>Last Name</label>
                                    <input type="text" class="form-control" name="lname" value="{{$app->last_name}}">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label>E-Mail</label>
                                    <input type="text" class="form-control" name="email" value="{{$app->email}}">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label>address</label>
                                    <input type="text" class="form-control" name="street" value="{{$app->street}}">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label>city</label>
                                    <input type="text" class="form-control" name="city" value="{{$app->city}}">
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label>State</label>
                                    <select class="form-control selectpicker" name="state">
                                        @foreach($regions['available_regions'] as $region)
                                            @if($region['id'] == $app->state)
                                            <option value="{{$region['id']}}" selected>{{$region['name']}}</option>
                                            @else
                                            <option value="{{$region['id']}}">{{$region['name']}}</option>
                                            @endif
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label>zip</label>
                                    <input type="text" class="form-control" name="zip" value="{{$app->zip}}">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label>Business EIN</label>
                                    <input type="text" class="form-control" name="ein" value="{{$app->ein}}">
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label>Resale #</label>
                                    <input type="text" class="form-control" name="resale" value="{{$app->resale_number}}">
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label>Cosmo #</label>
                                    <input type="text" class="form-control" name="cosmo" value="{{$app->cosmo_number}}">
                                </div>
                            </div>
                        </div>

                        @if ($app->status == 2)
                        <div class="row mt-5 text-right">
                            <div class="col-12">
                                <input style="font-weight: bold;" class="btn btn-primary" type="submit" name="action" value="Update And Reset Denied Status" />
                            </div>
                        </div>
                        @else
                        <div class="row mt-5 text-right">
                            <div class="col-12">
                                <input style="font-weight: bold;" class="btn btn-primary" type="submit" name="action" value="Update Application" />
                            </div>
                        </div>
                        @endif

                        @if ($app->status == null)
                        <div class="row mt-5 text-right">
                            <div class="col-12">
                                <input style="font-weight: bold;" class="btn btn-success" type="submit" name="action" value="Approve" />
                                <input style="font-weight: bold;" class="btn btn-danger" type="submit" name="action" value="Deny" />
                            </div>
                        </div>
                        @endif
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>


@if ($app->status != NULL && $app->status != 2 )
<div class="container-fluid mt-5">
    <div class="row">
        <div class="col-10 offset-sm-1">
            <div class="alert alert-primary" role="alert">
                Current User Count: <?=count($magUsers);?>
            </div>
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">Magento Tools</h3>
                </div>
                <div class="card-body">
                    <form method="POST" action="{{route('magento.newCustomer')}}">
                        @csrf
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>First Name</label>
                                    <input type="text" name="fname" class="form-control" placeholder="First Name" value="{{$app->first_name}}">
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>Last Name</label>
                                    <input type="text" name="lname" class="form-control" placeholder="Last name" value="{{$app->last_name}}">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label>E-Mail</label>
                                    <input type="text" name="email" class="form-control" placeholder="mail@mail.com" value="{{$app->email}}">
                                </div>
                            </div>
                        </div>
                        <input type="hidden" value="{{$app->id}}" name="appId">
                        <input type="hidden" value="{{$app->business_name}}" name="business_name">
                        <input type="submit" class="btn btn-primary" value="Generate User">
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endif

<div class="container" style="display:none;">
    <div class="row">
        <div class="col-12">
            <div class="card card-warning">
                <div class="card-header">
                    <h3 class="card-title">General Elements</h3>
                </div>
                <!-- /.card-header -->
                <div class="card-body">
                    <form method="POST" action="{{route('applications.update', $app->id)}}">
                        <input type="hidden" name="_method" value="PUT">
                        @csrf
                  <div class="row">
                    <div class="col-sm-6">
                      <!-- text input -->
                      <div class="form-group">
                        <label>Text</label>
                        <input type="text" class="form-control" placeholder="Enter ...">
                      </div>
                    </div>
                    <div class="col-sm-6">
                      <div class="form-group">
                        <label>Text Disabled</label>
                        <input type="text" class="form-control" placeholder="Enter ..." disabled="">
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-sm-6">
                      <!-- textarea -->
                      <div class="form-group">
                        <label>Textarea</label>
                        <textarea class="form-control" rows="3" placeholder="Enter ..."></textarea>
                      </div>
                    </div>
                    <div class="col-sm-6">
                      <div class="form-group">
                        <label>Textarea Disabled</label>
                        <textarea class="form-control" rows="3" placeholder="Enter ..." disabled=""></textarea>
                      </div>
                    </div>
                  </div>

                  <!-- input states -->
                  <div class="form-group">
                    <label class="col-form-label" for="inputSuccess"><i class="fas fa-check"></i> Input with
                      success</label>
                    <input type="text" class="form-control is-valid" id="inputSuccess" placeholder="Enter ...">
                  </div>
                  <div class="form-group">
                    <label class="col-form-label" for="inputWarning"><i class="far fa-bell"></i> Input with
                      warning</label>
                    <input type="text" class="form-control is-warning" id="inputWarning" placeholder="Enter ...">
                  </div>
                  <div class="form-group">
                    <label class="col-form-label" for="inputError"><i class="far fa-times-circle"></i> Input with
                      error</label>
                    <input type="text" class="form-control is-invalid" id="inputError" placeholder="Enter ...">
                  </div>

                  <div class="row">
                    <div class="col-sm-6">
                      <!-- checkbox -->
                      <div class="form-group">
                        <div class="form-check">
                          <input class="form-check-input" type="checkbox">
                          <label class="form-check-label">Checkbox</label>
                        </div>
                        <div class="form-check">
                          <input class="form-check-input" type="checkbox" checked="">
                          <label class="form-check-label">Checkbox checked</label>
                        </div>
                        <div class="form-check">
                          <input class="form-check-input" type="checkbox" disabled="">
                          <label class="form-check-label">Checkbox disabled</label>
                        </div>
                      </div>
                    </div>
                    <div class="col-sm-6">
                      <!-- radio -->
                      <div class="form-group">
                        <div class="form-check">
                          <input class="form-check-input" type="radio" name="radio1">
                          <label class="form-check-label">Radio</label>
                        </div>
                        <div class="form-check">
                          <input class="form-check-input" type="radio" name="radio1" checked="">
                          <label class="form-check-label">Radio checked</label>
                        </div>
                        <div class="form-check">
                          <input class="form-check-input" type="radio" disabled="">
                          <label class="form-check-label">Radio disabled</label>
                        </div>
                      </div>
                    </div>
                  </div>

                  <div class="row">
                    <div class="col-sm-6">
                      <!-- select -->
                      <div class="form-group">
                        <label>Select</label>
                        <select class="form-control">
                          <option>option 1</option>
                          <option>option 2</option>
                          <option>option 3</option>
                          <option>option 4</option>
                          <option>option 5</option>
                        </select>
                      </div>
                    </div>
                    <div class="col-sm-6">
                      <div class="form-group">
                        <label>Select Disabled</label>
                        <select class="form-control" disabled="">
                          <option>option 1</option>
                          <option>option 2</option>
                          <option>option 3</option>
                          <option>option 4</option>
                          <option>option 5</option>
                        </select>
                      </div>
                    </div>
                  </div>

                  <div class="row">
                    <div class="col-sm-6">
                      <!-- Select multiple-->
                      <div class="form-group">
                        <label>Select Multiple</label>
                        <select multiple="" class="form-control">
                          <option>option 1</option>
                          <option>option 2</option>
                          <option>option 3</option>
                          <option>option 4</option>
                          <option>option 5</option>
                        </select>
                      </div>
                    </div>
                    <div class="col-sm-6">
                      <div class="form-group">
                        <label>Select Multiple Disabled</label>
                        <select multiple="" class="form-control" disabled="">
                          <option>option 1</option>
                          <option>option 2</option>
                          <option>option 3</option>
                          <option>option 4</option>
                          <option>option 5</option>
                        </select>
                      </div>
                    </div>
                  </div>
                </form>
              </div>
              <!-- /.card-body -->
            </div>
        </div>
    </div>
</div>
@endsection
