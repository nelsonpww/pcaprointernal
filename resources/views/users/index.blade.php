@extends('layouts.dashboard')

@section('content')
<div class="container-fluid">
    <div class="row pt-4 pb-4">
        <div class="col-12">
            <h1 class="m-0 text-dark">All Magento Users</h1>
            <h5 class="text-muted">PLEASE NOTE: Load time may or may not be slow as live infromation has to load per user</h5>
        </div>
    </div>
</div>

<div class="container-fluid">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                  <h3 class="card-title">All</h3>
                </div>
                <div class="card-body">
                    <table id="users_table" class="table table-striped table-bordered dt-responsive nowrap" style="width:100%">
                        <thead>
                            <tr>
                                <th>Magento Id</th>
                                <th>Business Name</th>
                                <th>Name</th>
                                <th>Email</th>
                                <th>Generated At</th>
                                <th>Password</th>
                                <th class="text-center">Action</th>
                            </tr>
                        </thead>
                        <tbody>

                            @foreach ($magUsers as $user)
                            <tr>
                                <td>{{ $user->user_id }}</td>
                                <td>{{ $user->business_name }}</td>
                                <td>{{ $user->firstname }} {{ $user->lastname }}</td>
                                <td>{{ $user->email }}</td>
                                <td><?php echo isset( $user->created_at) ? $user->created_at->format('m/d/Y h:i a') : '';?></td>
                                <td>{{ $user->password }}</td>
                                <td class="text-center">
                                    <a class="btn btn-primary" href="{{route('users.edit', $user->user_id )}}"><i class="fa fa-eye"></i> View</a>
                                    <!-- <a class="btn btn-danger" href="#"><i class="glyphicon glyphicon-align-left"></i> Delete</a> -->
                                </td>
                            </tr>
                            @endforeach

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
