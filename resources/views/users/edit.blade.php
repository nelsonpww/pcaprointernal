@extends('layouts.dashboard')

@section('content')

@if(  Session::has('msg') )
<div class="alert alert-primary" role="alert">
    <strong>{{ Session::get('msgTitle') }}</strong> {{ Session::get('msg') }}
</div>
@endif

@if( Session::has('msgE') )
<div class="alert alert-danger" role="alert">
    <strong>{{ Session::get('msgTitleE') }}</strong> {{ Session::get('msgE') }}
</div>
@endif

<div class="container-fluid">
    <div class="row pt-4 pb-4">
        <div class="col-12">
            <h1 class="m-0 text-dark">{{$magentoUser->firstname}} {{$magentoUser->lastname}}: {{$magentoUser->business_name}}</h1>
        </div>
    </div>
</div>

<div class="container-fluid">
    <div class="row">
        <div class="col-10 offset-sm-1">
            <div class="card card-warning">
                <div class="card-header">
                    <h3 class="card-title font-weight-bold">Generated On: {{$magentoUser->created_at->format('m/d/Y')}}</h3>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="container-fluid">
    <div class="row">
        <div class="col-10 offset-sm-1">
            <div class="card card-warning">
                <div class="card-header">
                    <h3 class="card-title font-weight-bold">Magento Customer Information</h3>
                </div>
                <div class="card-body">
                    <form method="POST" action="{{ route('magento.updateCustomer') }}">
                        @csrf
                        <div class="row">
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label>First Name</label>
                                    <input type="text" class="form-control" name="fname" value="{{$customerInfo->firstname}}">
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label>Last Name</label>
                                    <input type="text" class="form-control" name="lname" value="{{$customerInfo->lastname}}">
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label>Email</label>
                                    <input type="email" class="form-control" name="email" value="{{$customerInfo->email}}">
                                    <input type="hidden" class="form-control" name="magId" value="{{$customerInfo->id}}">
                                </div>
                            </div>
                        </div>
                        <?php// dd($customerInfo); ?>
                        @foreach($customerInfo->addresses as $address)
                        <?php // dd($address); ?>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>Company Name</label>
                                    <input type="text" class="form-control" name="company_name" value="{{$address->company}}">
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>Telephone</label>
                                    <input type="text" class="form-control" name="telephone" value="{{$address->telephone}}">
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label>Street Address</label>
                                    <input type="text" class="form-control" name="street" value="{{$address->street[0]}}">
                                    <input type="hidden" class="form-control" name="address_id" value="{{$address->id}}">
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label>City</label>
                                    <input type="text" class="form-control" name="city" value="{{$address->city}}">
                                    echo {{$address->region_id}}
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label>State</label>
                                    <select class="form-control selectpicker" name="state">
                                        @foreach($regions['available_regions'] as $region)
                                            @if($region['id'] == $address->region_id)
                                            <option value="{{$region['id']}}" selected>{{$region['name']}}</option>
                                            @else
                                            <option value="{{$region['id']}}">{{$region['name']}}</option>
                                            @endif
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label>Zip</label>
                                    <input type="text" class="form-control" name="zip" value="{{$address->postcode}}">
                                </div>
                            </div>
                            <div class="col-12" style="margin-left:20px;">
                                <div class="form-group">
                                    <input type="checkbox" name="billing" class="form-check-input" id="exampleCheck1" <?php echo ($address->default_billing == '1' ? 'checked' : '');?> />
                                    <label class="form-check-label" for="exampleCheck1">Default Billing</label>
                                    <br>
                                    <input type="checkbox" name="shipping" class="form-check-input" id="exampleCheck2" <?php echo $address->default_shipping == '1' ? 'checked' : '';?> />
                                    <label class="form-check-label" for="exampleCheck2">Default Shipping</label>
                                </div>
                            </div>
                        </div>
                        @endforeach
                        <div class="row mt-5 text-right">
                            <div class="col-12">
                                <input style="font-weight: bold;" class="btn btn-success" type="submit" name="action" value="Update Magento" />
                            </div>
                        </div>
                    </form>
                    <div class="row">
                        <div class="col-12">
                            <h3>Password</h3>
                        </div>
                            <form method="POST" action="{{ route('magento.resetCustomerPassword') }}">
                                @csrf
                                <div class="col-12">
                                    <div class="form-group">
                                    <input type="hidden" name="email" value="{{$customerInfo->email}}" />
                                    <input style="font-weight: bold;" class="btn btn-danger" type="submit" name="action" value="Send Reset Link" />
                                </div>
                            </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="container-fluid">
    <div class="row pt-4 pb-4">
        <div class="col-12">
            <h1 class="m-0 text-dark">Order History</h1>
        </div>
    </div>
</div>

<div class="container-fluid">
    <div class="row">
        <div class="col-10 offset-sm-1">
            <div class="card card-warning">
                <div id="accordion">
                    <?php// echo var_dump($orders); ?>
                    @foreach($orders->items as $order)
                    <?php// dd($order); ?>
                    <div class="card-header">
                        <h3 class="card-title font-weight-bold" style="float:none;">Order# {{ $order->increment_id }} - Placed at: <?php echo date('m/d/y h:i A', strtotime($order->created_at));?><i class="fa fa-plus" style="float:right;"></i></h3>
                    </div>
                    <div class="card-body">
                        <div class="container">
                            <div class="row">
                                <div class="col-12">
                                    <!-- info row -->
                                    <div class="row invoice-info">
                                        <div class="col-sm-4 invoice-col">
                                            <strong>Delivery Address</strong>
                                            <address>
                                                {{$order->extension_attributes->shipping_assignments[0]->shipping->address->company}}<br>
                                                <?php// dd($order); ?>
                                                {{$order->extension_attributes->shipping_assignments[0]->shipping->address->city}}<br>
                                                {{$order->extension_attributes->shipping_assignments[0]->shipping->address->street[0]}}, {{$order->extension_attributes->shipping_assignments[0]->shipping->address->region_code}} {{$order->extension_attributes->shipping_assignments[0]->shipping->address->postcode}}<br>
                                                Phone: {{$order->extension_attributes->shipping_assignments[0]->shipping->address->telephone}}<br>
                                                Email: {{$order->extension_attributes->shipping_assignments[0]->shipping->address->email}}
                                            </address>
                                        </div>
                                        <!-- /.col -->
                                        <div class="col-sm-4 invoice-col">
                                            <b>Order#:</b> {{ $order->increment_id }}
                                            <br>
                                            <br>
                                            <b>Total Amount:</b>$ <?php echo number_format($order->total_due, 2);?><br>
                                            <b>Total Qty:</b> {{$order->total_qty_ordered}}
                                        </div>
                                        <!-- /.col -->
                                      </div>
                                      <!-- /.row -->

                                      <!-- Table row -->
                                      <div class="row">
                                        <div class="col-12 table-responsive">
                                          <table class="table table-striped">
                                            <thead>
                                            <tr>
                                              <th>Qty</th>
                                              <th>Product</th>
                                              <th>Sku #</th>
                                              <th>Price ea.</th>
                                              <th>Subtotal</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                                @foreach($order->items as $item)
                                                <tr>
                                                    <td>{{$item->qty_ordered}}</td>
                                                    <td>{{$item->name}}</td>
                                                    <td>{{$item->sku}}</td>
                                                    <td>$ <?php echo number_format($item->price, 2); ?></td>
                                                    <td>$ <?php echo number_format($item->row_total, 2); ?></td>
                                                </tr>
                                                @endforeach
                                            </tbody>
                                          </table>
                                        </div>
                                        <!-- /.col -->
                                      </div>
                                      <!-- /.row -->

                                      <div class="row">
                                        <!-- accepted payments column -->
                                        <div class="col-6">
                                            <h4>Payment Method:</h4>
                                            <p class="lead">{{$order->payment->additional_information[0]}}</p>
                                            <!-- <img src="../../dist/img/credit/visa.png" alt="Visa">-->
                                            <p class="text-muted well well-sm shadow-none" style="margin-top: 10px;">
                                                <strong>Shipping Method:</strong> {{$order->shipping_description}}<br>
                                            </p>
                                        </div>
                                        <!-- /.col -->
                                        <div class="col-6">
                                          <p class="lead">Order Details</p>

                                          <div class="table-responsive">
                                            <table class="table">
                                              <tbody><tr>
                                                <th style="width:50%">Subtotal:</th>
                                                <td>$ <?php echo number_format($order->subtotal, 2);?></td>
                                              </tr>
                                              <tr>
                                                <th>Tax</th>
                                                <td>$ <?php echo number_format($order->tax_amount, 2);?></td>
                                              </tr>
                                              <tr>
                                                <th>Shipping:</th>
                                                <td>$ <?php echo number_format($order->shipping_amount, 2);?></td>
                                              </tr>
                                              <tr>
                                                <th>Total:</th>
                                                <td>$ <?php echo number_format($order->total_due, 2);?></td>
                                              </tr>
                                            </tbody></table>
                                          </div>
                                        </div>
                                        <!-- /.col -->
                                      </div>
                                      <!-- /.row -->

                                      <!-- this row will not appear when printing -->
                                      <!-- <div class="row no-print">
                                        <div class="col-12">
                                          <a href="invoice-print.html" target="_blank" class="btn btn-default"><i class="fas fa-print"></i> Print</a>
                                          <button type="button" class="btn btn-primary float-right" style="margin-right: 5px;">
                                            <i class="fas fa-download"></i> Print Order
                                          </button>
                                        </div>
                                      </div> -->
                                    </div>
                                </div>
                            </div>
                    </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</div>

<div class="container" style="display:none">
    <div class="row">
        <div class="col-12">
            <div class="card card-warning">
                <div class="card-header">
                    <h3 class="card-title">General Elements</h3>
                </div>
                <!-- /.card-header -->
                <div class="card-body">
                    <form method="POST" action="{{route('applications.update', $magentoUser->id)}}">
                        <input type="hidden" name="_method" value="PUT">
                        @csrf
                  <div class="row">
                    <div class="col-sm-6">
                      <!-- text input -->
                      <div class="form-group">
                        <label>Text</label>
                        <input type="text" class="form-control" placeholder="Enter ...">
                      </div>
                    </div>
                    <div class="col-sm-6">
                      <div class="form-group">
                        <label>Text Disabled</label>
                        <input type="text" class="form-control" placeholder="Enter ..." disabled="">
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-sm-6">
                      <!-- textarea -->
                      <div class="form-group">
                        <label>Textarea</label>
                        <textarea class="form-control" rows="3" placeholder="Enter ..."></textarea>
                      </div>
                    </div>
                    <div class="col-sm-6">
                      <div class="form-group">
                        <label>Textarea Disabled</label>
                        <textarea class="form-control" rows="3" placeholder="Enter ..." disabled=""></textarea>
                      </div>
                    </div>
                  </div>

                  <!-- input states -->
                  <div class="form-group">
                    <label class="col-form-label" for="inputSuccess"><i class="fas fa-check"></i> Input with
                      success</label>
                    <input type="text" class="form-control is-valid" id="inputSuccess" placeholder="Enter ...">
                  </div>
                  <div class="form-group">
                    <label class="col-form-label" for="inputWarning"><i class="far fa-bell"></i> Input with
                      warning</label>
                    <input type="text" class="form-control is-warning" id="inputWarning" placeholder="Enter ...">
                  </div>
                  <div class="form-group">
                    <label class="col-form-label" for="inputError"><i class="far fa-times-circle"></i> Input with
                      error</label>
                    <input type="text" class="form-control is-invalid" id="inputError" placeholder="Enter ...">
                  </div>

                  <div class="row">
                    <div class="col-sm-6">
                      <!-- checkbox -->
                      <div class="form-group">
                        <div class="form-check">
                          <input class="form-check-input" type="checkbox">
                          <label class="form-check-label">Checkbox</label>
                        </div>
                        <div class="form-check">
                          <input class="form-check-input" type="checkbox" checked="">
                          <label class="form-check-label">Checkbox checked</label>
                        </div>
                        <div class="form-check">
                          <input class="form-check-input" type="checkbox" disabled="">
                          <label class="form-check-label">Checkbox disabled</label>
                        </div>
                      </div>
                    </div>
                    <div class="col-sm-6">
                      <!-- radio -->
                      <div class="form-group">
                        <div class="form-check">
                          <input class="form-check-input" type="radio" name="radio1">
                          <label class="form-check-label">Radio</label>
                        </div>
                        <div class="form-check">
                          <input class="form-check-input" type="radio" name="radio1" checked="">
                          <label class="form-check-label">Radio checked</label>
                        </div>
                        <div class="form-check">
                          <input class="form-check-input" type="radio" disabled="">
                          <label class="form-check-label">Radio disabled</label>
                        </div>
                      </div>
                    </div>
                  </div>

                  <div class="row">
                    <div class="col-sm-6">
                      <!-- select -->
                      <div class="form-group">
                        <label>Select</label>
                        <select class="form-control">
                          <option>option 1</option>
                          <option>option 2</option>
                          <option>option 3</option>
                          <option>option 4</option>
                          <option>option 5</option>
                        </select>
                      </div>
                    </div>
                    <div class="col-sm-6">
                      <div class="form-group">
                        <label>Select Disabled</label>
                        <select class="form-control" disabled="">
                          <option>option 1</option>
                          <option>option 2</option>
                          <option>option 3</option>
                          <option>option 4</option>
                          <option>option 5</option>
                        </select>
                      </div>
                    </div>
                  </div>

                  <div class="row">
                    <div class="col-sm-6">
                      <!-- Select multiple-->
                      <div class="form-group">
                        <label>Select Multiple</label>
                        <select multiple="" class="form-control">
                          <option>option 1</option>
                          <option>option 2</option>
                          <option>option 3</option>
                          <option>option 4</option>
                          <option>option 5</option>
                        </select>
                      </div>
                    </div>
                    <div class="col-sm-6">
                      <div class="form-group">
                        <label>Select Multiple Disabled</label>
                        <select multiple="" class="form-control" disabled="">
                          <option>option 1</option>
                          <option>option 2</option>
                          <option>option 3</option>
                          <option>option 4</option>
                          <option>option 5</option>
                        </select>
                      </div>
                    </div>
                  </div>
                </form>
              </div>
              <!-- /.card-body -->
            </div>
        </div>
    </div>
</div>
@endsection
