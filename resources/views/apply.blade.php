@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            @if(Session::has('success'))
                <div class="alert alert-success">
                    {{Session::get('success')}}
                </div>
            @endif
        </div>
    </div>
    <div class="row">
        <div class="col-md-12 mb-4 pb-4 text-center">
            <h1>Front End Business Application</h1>
        </div>
        <div class="col-md-12">
            @if ( !$errors->isEmpty() )
            <div class="alert alert-danger">
                <h3>Please fix the following:</h3>
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
            @endif

            <form method="POST" action="{{ route('sendApplyForm') }}" enctype="multipart/form-data">
                @csrf
                <div class="form-row">
                    <div class="form-group col-md-4">
                        <label>First Name</label>
                        <input type="text" class="form-control @if($errors->has('firstName')) is-invalid @endif" name="firstName" placeholder="First Name" value="{{ old('firstName') }}">
                    </div>
                    <div class="form-group col-md-4">
                        <label>Last Name</label>
                        <input type="text" class="form-control @if($errors->has('lastName')) is-invalid @endif" name="lastName" placeholder="Last Name" value="{{ old('lastName') }}">
                    </div>
                    <div class="form-group col-md-4">
                        <label>E-Mail</label>
                        <input type="text" class="form-control @if($errors->has('email')) is-invalid @endif" name="email" placeholder="mail@mail.com" value="{{ old('email') }}">
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-4">
                        <label>Business Name</label>
                        <input type="text" class="form-control @if($errors->has('business')) is-invalid @endif" name="business" placeholder="Company Name LLC" value="{{ old('business') }}">
                    </div>
                    <div class="form-group col-md-4">
                        <label>EIN</label>
                        <input type="text" class="form-control @if($errors->has('ein')) is-invalid @endif" name="ein" placeholder="ein" value="{{ old('ein') }}">
                    </div>
                    <div class="form-group col-md-4">
                        <label>Telephone #</label>
                        <input type="text" class="form-control @if($errors->has('telephone')) is-invalid @endif" name="telephone" placeholder="###-###-####" value="{{ old('telephone') }}">
                    </div>
                </div>
                <div class="form-row">
                    <h5>Address</h5>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-12">
                        <label>street</label>
                        <input type="text" class="form-control @if($errors->has('address_street')) is-invalid @endif" name="address_street" placeholder="123 Street Rd" value="{{ old('address_street') }}">
                    </div>
                    <div class="form-group col-md-4">
                        <label>City</label>
                        <input type="text" class="form-control @if($errors->has('address_street')) is-invalid @endif" name="address_city" placeholder="city" value="{{ old('address_city') }}">
                    </div>
                    <div class="form-group col-md-4">
                        <label>State</label>
                        <select class="form-control selectpicker @if($errors->has('address_street')) is-invalid @endif" name="address_state">
                        @foreach($regions['available_regions'] as $region)
                            <option value="{{$region['id']}}">{{$region['name']}}</option>
                        @endforeach
                        </select>
                    </div>
                    <div class="form-group col-md-4">
                        <label>Zip</label>
                        <input type="text" class="form-control @if($errors->has('address_zip')) is-invalid @endif" name="address_zip" placeholder="zip" value="{{ old('address_zip') }}">
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label>NY Resale Certificate #</label>
                        <input type="text" class="form-control @if($errors->has('resale')) is-invalid @endif" name="resale" placeholder="99-9999999" value="{{ old('resale') }}">
                    </div>
                    <div class="form-group col-md-6">
                        <label>Cosemtology License #</label>
                        <input type="text" class="form-control @if($errors->has('cosemtology')) is-invalid @endif" name="cosemtology" placeholder="license #" value="{{ old('cosemtology') }}">
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label for="w9form">W9 Form</label>
                        <input type="file" name="w9" class="form-control-file" id="w9" accept="application/pdf" value="{{ old('w9form') }}" required>
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-12 text-right">
                        <button type="submit" class="btn btn-primary">Apply Now</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection
