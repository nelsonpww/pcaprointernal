@extends('layouts.dashboard')

@section('content')
<div class="container-fluid">
    <div class="row pt-4 pb-4">
        <div class="col-12">
            <h1 class="m-0 text-dark">Dashboard</h1>
        </div>
    </div>
</div>

<div class="container-fluid">
    <div class="row">
      <div class="col-lg-2 col-6">
        <!-- small box -->
        <div class="small-box">
          <div class="inner">
            <h3>{{$newApps}}</h3>

            <p>New Applications</p>
          </div>
          <div class="icon">
            <i class="ion ion-bag"></i>
          </div>
          <a href="#" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
        </div>
      </div>
      <!-- ./col -->
      <div class="col-lg-2 col-6">
        <!-- small box -->
        <div class="small-box">
          <div class="inner">
            <h3>{{$approvedApps}}</h3>
            <!-- <h3>{{$approvedApps}}<sup style="font-size: 20px">%</sup></h3> -->

            <p>Approved Applications</p>
          </div>
          <div class="icon">
            <i class="ion ion-stats-bars"></i>
          </div>
          <a href="#" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
        </div>
      </div>
      <!-- ./col -->
      <div class="col-lg-2 col-6">
        <!-- small box -->
        <div class="small-box">
          <div class="inner">
            <h3>{{$deniedApps}}</h3>

            <p>Denied Applications</p>
          </div>
          <div class="icon">
            <i class="ion ion-person-add"></i>
          </div>
          <a href="#" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
        </div>
      </div>
      <!-- ./col -->
      <div class="col-lg-2 col-6">
        <!-- small box -->
        <div class="small-box">
          <div class="inner">
            <h3>{{$activeMagento}}</h3>

            <p>Active Users</p>
          </div>
          <div class="icon">
            <i class="ion ion-pie-graph"></i>
          </div>
          <a href="#" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
        </div>
      </div>
      <!-- ./col -->
    </div>
</div>

<div class="container-fluid">
    <div class="row">
        <div class="col-12">

        </div>
    </div>
</div>
@endsection
