<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth/login');
})->middleware('checkip');

Auth::routes();

Route::get('/dashboard', 'DashboardController@index')->name('dashboard')->middleware('auth');
Route::get('/dashboard/logout', 'DashboardController@logout')->name('dashboard.logout');
Route::post('/dashboard/testFile', 'DashboardController@testFile')->name('testFile');

Route::resource('/dashboard/users', 'UsersController')->middleware('auth');
Route::resource('/dashboard/applications', 'ApplicationsController')->middleware('auth');
// Route::resource('/applications', 'ApplicationsController');

Route::get('/apply', 'ApplicationsController@apply')->name('apply');
Route::post('/sendApplyForm', 'ApplicationsController@sendApplyForm')->name('sendApplyForm');
Route::post('/fileDown','ApplicationsController@down')->name('down');


Route::get('/magento/countries', 'MagentoController@getMagentoCountries')->middleware('auth');
Route::post('/magento/newCustomer', 'MagentoController@createMagentoCustomer')->middleware('auth')->name('magento.newCustomer');
Route::post('/magento/updateCustomer', 'MagentoController@updateMagentoCustomer')->middleware('auth')->name('magento.updateCustomer');
Route::post('/magento/resetCustomerPassword', 'MagentoController@resetCustomerPassword')->middleware('auth')->name('magento.resetCustomerPassword');
//Route::get('/magento/customers', 'MagentoController@getAllMagentoCustomers');

Route::get('/error/404', 'ErrorController@show404')->name('error404');

Route::get('/mail', 'ErrorController@mail');

Route::get('/home', function () {
    return redirect('/dashboard');
});
