$(document).ready(function() {

    $('#table_id').DataTable({
        order: [[ 7, "desc" ]],
        columnDefs: [
           { orderable: false, targets: 0 }
        ]
    });

    $('#users_table').DataTable({
        order: [[ 4, "desc" ]],
        columnDefs: [
           { orderable: false, targets: 0 }
        ]
    });

      // $('#example2').DataTable({
      //   "paging": true,
      //   "lengthChange": false,
      //   "searching": false,
      //   "ordering": true,
      //   "info": true,
      //   "autoWidth": false,
      // });


    setTimeout(function(){
        $("div.alert-success").remove();
    }, 3000 );

    $('#dashboard form').on('keyup keypress', function(e) {
        var keyCode = e.keyCode || e.which;
        if (keyCode === 13) {
            e.preventDefault();
            return false;
        }
    });

      $( "#accordion" ).accordion({
        active: false,
        collapsible: true
    });
});
